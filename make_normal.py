#!/usr/bin/env python
#-*- coding: utf-8 -*-

#import libraries
import numpy as np
import sys
import copy
import os
import platform as pf

# global variable
smoothing_level = 3  # what time we will smooth the time-series-data(TSD)
wma_width = 7  # [wma_width] points weighted moving average



class Normalize:

    # make structure "self"
    def __init__(self, src, s, n):
        self.readTxt(src)
        self.norm(self.orate)
        if s == "1":
            self.nrate = self.smooth(self.nrate)
        if n == "1":
            self.nrate = self.derive(self.nrate)


    def readTxt(self, src):

        with open(src, 'r') as f:
            lines = f.readlines()

        times, rates = zip(*[l.split() for l in lines])
        
        self.otime = map(float, times)
        self.orate = map(float, rates)
        

    def norm(self, src):
        norm_rate = []
        max_rate = np.max(src)
        for i in src:
            norm_rate.append(i / max_rate)
        self.nrate = norm_rate


    # smoothing(in [smoothing_level] time)
    def smooth(self, src):
        global wma_width
        global smoothing_level
        s = copy.copy(src)
        for i in range(smoothing_level):
            s = self.wma_err(s, wma_width)        
        return s

    
    # Weighted Moving Average([n] points)
    #     [n] is odd number
    def wma_err(self, src, n):
        s = copy.copy(src)
        n0 = int(n / 2)

        # both ends
        for i in range(1, n0):
            l_end = src[i] * float(n0 + 1)
            r_end = src[-(i + 1)] * float(n0 + 1)
            l_weightsum = float(n0 + 1)
            r_weightsum = float(n0 + 1)
            for j in range(1, i + 1):
                l_end += (src[i - j] + src[i + j]) * float(n0 + 1 - j)
                r_end += (src[-(i - j + 1)] + src[-(i + j + 1)]) * float(n0 + 1 - j)
                l_weightsum += float(n0 + 1 - j) * 2
                r_weightsum += float(n0 + 1 - j) * 2
            s[i] = l_end / l_weightsum
            s[-(i + 1)] = r_end / r_weightsum

        # except ends
        for i in range(n0, len(src) - n0):
            t = src[i] * float(n0 + 1)
            weightsum = float(n0 + 1)
            for j in range(1, n0 + 1):
                t += (src[i - j] + src[i + j]) * float(n0 + 1 - j)
                weightsum +=  float(n0 + 1 - j) * 2
            s[i] = t / weightsum

        return s


    # for Derivative DTW
    def derive(self, src):
        s = copy.copy(src)
        n = 3
        nhalf = (n - 1) / 2
        lim = 0
        tmp = 0.0

        for i in range(1, len(s) - 1):
            if i < nhalf:
                lim = i
            elif i > len(s) - (nhalf + 1):
                lim = len(s) - 1 - i
            else:
                lim = nhalf
            for j in range(1, lim + 1):
                tmp += ( ((src[i] - src[i - j]) / float(j)) + ((src[i + j] - src[i - j]) / float(j * 2)) ) / 2.0
            s[i] = tmp / lim
            tmp = 0.0

        s[0] = s[1]
        s[len(s) - 1] = s[len(s) - 2]

        return s



if __name__=='__main__':

    if len(sys.argv) != 4:
        print("Usage: # python {0} data-directory s-option(0 or 1) d-option(0 or 1)".format(sys.argv[0]))
        quit()

    # Determine the name of output directory.
    is_smoothed   = "notsmoothed" if sys.argv[2] == "0" else "smoothed"
    is_derivative = "DTW" if sys.argv[3] == "0" else "DDTW"
    data_type =  is_smoothed + is_derivative


    # Get in/out dirctory name.
    input_dir = sys.argv[1]
    
    normalized_dir = "normalized"
    if os.path.exists(normalized_dir) == False:
        os.mkdir(normalized_dir)
        
    output_dir = os.path.join(normalized_dir, data_type)
    if os.path.exists(output_dir) == False:
        os.mkdir(output_dir)

    # Normalize!
    objects = os.listdir(input_dir)
    
    for obj in objects:
        n = Normalize(os.path.join(input_dir, obj), sys.argv[2], sys.argv[3])
        
        with open(os.path.join(output_dir, obj), 'w') as f_out:
            for i in range(len(n.otime)):
                f_out.write("{0:.1f} {1:.5f}\n".format(n.otime[i], n.nrate[i]))

        print("{0} is normalized\n".format(obj[: -4]))

