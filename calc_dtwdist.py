#!/usr/bin/env python
#-*- coding: utf-8 -*-

#import libraries
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import dtw
import sys
import copy
import os
import itertools
import re


class TimeSeries:

    def __init__(self, src):
        self.readTxt(src)
        

    def readTxt(self, src):

        with open(src, 'r') as f:
            lines = f.readlines()

        times, rates = zip(*[l.split() for l in lines])
        
        self.otime = map(float, times)
        self.orate = map(float, rates)

        
        
def plot(ts1, ts2, output_dir, obname1, obname2):

    s = itertools.izip_longest(ts1.orate, ts2.orate, fillValue = float("Nan"))
    
    plt.clf()
    plt.xlim([0, len(s)])
    plt.ylim([-0.3, 1.0])

    plt.plot(s[0], 'r-', label="obname1")
    plt.plot(s[1], 'b-', label="obname2")

    plt.suptitle("Comparing: {0}, {1}".format(obname1, obname2))
    plt.savefig(os.path.join(output_dir, "rate", (obname1 + "__" + obname2 + ".png")))


    
if __name__=='__main__':

    flag_plot = False
    

    if len(sys.argv) != 2:
        print("Usage: # python %s input_dir" % sys.argv[0])
        quit()

    # Get input files list.
    input_path = sys.argv[1]
    objects = sorted(os.listdir(input_path))

    # Get output CSV directory.
    out_dir = "result"
    if os.path.exists(out_dir) == False:
        os.mkdir(out_dir)

    # Open CSV file.
    fo = open(os.path.join(out_dir, ("DTW_" + os.path.basename(input_path) + ".csv")), 'w')
    fo.write("Object1,Object2,DTW-Distance\n")

    
    input_type = "DDTW" if input_path.find("DDTW") >= 0 else "DTW"
    out_fig_dir = os.path.join(out_dir, "compares", input_type)

        
    # culcurate DTW distance in all combination of outbursts
    for x in range(len(objects)):

        ts1 = TimeSeries(os.path.join(input_path, objects[x]))

        for y in range(x + 1, len(objects)):

            ts2 = TimeSeries(os.path.join(input_path, objects[y]))

            # if you want only [dist],
            # dist = dtw.dtw(ts1.orate, ts2.orate, dist_only = True)
            dist, cost, path = dtw.dtw(ts1.orate, ts2.orate, dist_only = False)

            # Normalize with length.
            dist /= float(max(len(ts1.otime), len(ts2.otime)))

            # Output result to CSV.
            fo.write("%s,%s,%f\n" % (objects[x][: -4], objects[y][: -4], dist))
            print("Compare [%s], [%s]: %f\n" % (objects[x][: -4], objects[y][: -4], dist))

            
            if flag_plot:
                
                # plot normalized, smoothed rate (use if you want)
                plot(ts1, ts2, out_fig_dir, objects[x][: -4], objects[y][: -4])

                # plot DTW-path (use if you want)
                plt.clf()
                fig = plt.figure(1)
                ax = fig.add_subplot(111)
                plot1 = plt.imshow(cost.T, origin='lower', cmap=cm.gray, interpolation='nearest')
                plot2 = plt.plot(path[0], path[1], 'w')
                xlim = ax.set_xlim((-0.5, cost.shape[0]-0.5))
                ylim = ax.set_ylim((-0.5, cost.shape[1]-0.5))
                
                plt.savefig(os.path.join(out_fig_dir, "path", (objects[x][: -4] + "__" + objects[y][: -4] + ".png")))
            
    fo.close()

